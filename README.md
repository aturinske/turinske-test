# Turinske Test

If a [Turing Test](https://en.wikipedia.org/wiki/Turing_test) is humans interfacing with computers to see if they are human-like,
then a [Turinske Test](https://souldzin.gitlab.io/turinske-test/) is a simulation interfacing with humans to see if they are simulation-like.

## Features

| Feature                                            | Screenshot                                    |
| -------------------------------------------------- | --------------------------------------------- |
| Engage in life-like conversations!                 | ![suffering](./docs/suffering.png)            |
| Determine the likelihood of being in a simulation! | ![results](./docs/results.png)                |
| You can even talk to animals!                      | ![talking to fox](./docs/talk_to_animals.png) |

## What's a Turinske?

[Here's one](https://about.gitlab.com/company/team/#aturinske).
