import * as PIXI from "pixi.js";
import { GAME_FONT, TEXT_BOX_SHADOW_HEIGHT } from "../constants";

type TextBoxOptions = {
  text?: string;
  fontColor: number;
  fontSize: number;
  width: number;
  minHeight: number;
  bgColor: number;
  paddingY: number;
  paddingX: number;
};

const createDialogContainer = ({
  width,
  minHeight,
  bgColor,
  paddingY,
}: TextBoxOptions) => {
  const container = new PIXI.Container();
  const borderSize = 4;
  let height = minHeight;

  var textbg = new PIXI.Graphics();

  const draw = () => {
    textbg.clear();
    textbg.beginFill(0x000000, 0.25);
    textbg.drawRoundedRect(20, TEXT_BOX_SHADOW_HEIGHT, width - 40, height, 20);

    textbg.beginFill(bgColor, 1.0);
    textbg.drawRoundedRect(0, 0, width, height, 20);
    textbg.endFill();

    textbg.lineStyle(borderSize, 0xffffff);
    textbg.drawRoundedRect(0, 0, width, height, 20);
  };

  container.addChild(textbg);
  draw();

  const setHeight = (newHeightArg: number) => {
    const newHeight = newHeightArg + paddingY * 2;
    height = Math.max(minHeight, newHeight);

    draw();
  };

  return { container, setHeight };
};

export const createTextBox = (options: TextBoxOptions) => {
  const { fontSize, fontColor, width, paddingX, paddingY, text } = options;
  const { container, setHeight } = createDialogContainer(options);

  const textWidth = width - paddingX * 2;

  var tx = new PIXI.Text("", {
    fontFamily: GAME_FONT,
    fontSize,
    fill: fontColor,
    align: "left",
    wordWrap: true,
    wordWrapWidth: textWidth,
  });
  tx.position.set(paddingX, paddingY);
  tx.anchor.set(0, 0);

  const setText = (newText: string) => {
    tx.text = newText;
    setHeight(tx.height);
  };

  if (text) {
    setText(text);
  }

  container.addChild(tx);

  return { container, setText };
};
