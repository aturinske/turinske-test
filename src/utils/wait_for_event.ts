export const waitForEvent = (
  thing: { once: (evt: string, fn: Function) => any },
  evt: string
) =>
  new Promise((resolve) => {
    thing.once(evt, resolve);
  });
