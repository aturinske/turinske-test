import BACKGROUND_PNG from "../assets/background.png";
import TILE_PNG from "../assets/bg_tile.png";
import DANIELLE_PNG from "../assets/danielle.png";
import FOX_PNG from "../assets/fox.png";
import MAYA_PNG from "../assets/maya.png";
import PARROT_PNG from "../assets/parrot.png";
import TYLER_PNG from "../assets/tyler.png";
import XANDER_PNG from "../assets/xander_lea.png";

export {
  BACKGROUND_PNG,
  TILE_PNG,
  DANIELLE_PNG,
  FOX_PNG,
  MAYA_PNG,
  PARROT_PNG,
  TYLER_PNG,
  XANDER_PNG,
};
